#!/bin/bash
systemctl stop mariadb.service
#move mysql data to the partition
rsync  -a /var/lib/mysql/* /srv/mysql/data/
systemctl start mariadb.service
#show cluster status
mysql -e "SHOW GLOBAL STATUS LIKE 'wsrep_cluster_size';"
tune2fs -m1 /dev/sda3
tune2fs -m0 /dev/sdb
