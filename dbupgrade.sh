#!/bin/bash
vers="$1"
if [ -z "$vers" ] ; then
	echo "I need a param..."
	exit 1
else
	if [[ "$vers" -eq 1 ]] || [[ "$vers" -eq 2 ]] ; then 
		mback=mariadb-backup-10."$vers"
		gal_vers=3
	elif [[ "$vers" -eq 3 ]] ; then
		mback=mariadb-backup
		gal_vers=3
	else
		mback=mariadb-backup
		gal_vers=4
	fi
fi 


mysql -e 'SELECT table_schema "DB", ROUND(SUM(data_length + index_length) / 1024 / 1024, 1) "MB" FROM information_schema.tables GROUP BY table_schema; ' | grep testdb ; df -h | egrep 'sda3|sdb'
mysql -e "SHOW GLOBAL STATUS LIKE 'wsrep_cluster_size';"

sed -i -e "s/10../10."$vers"/g" /etc/apt/sources.list.d/php.list
mysql -e "SET GLOBAL innodb_fast_shutdown=0;"
mysql -e "SHOW GLOBAL variables LIKE 'innodb_fast_shutdown';"
sleep 5
apt-get update
systemctl stop mariadb.service
DEBIAN_FRONTEND=noninteractive apt-get -y remove mariadb-server "$mback" galera-"$gal_vers"
apt-get -y install mariadb-server "$mback" galera-"$gal_vers"

ps auxf | grep mysql
mysql_upgrade --skip-write-binlog
mysql -e "SHOW GLOBAL STATUS LIKE 'wsrep_cluster_size';"
mysql -e "SHOW GLOBAL variables LIKE 'wsrep_sst_method';"
#to change sst method:
#SET GLOBAL wsrep_sst_method='mariabackup';
