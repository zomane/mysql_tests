#!/bin/bash
systemctl stop mariadb.service
#move mysql data to the partition
rsync  -a /var/lib/mysql/* /srv/mysql/data/
galera_new_cluster
mysql -e "SET GLOBAL wsrep_sst_auth = 'wsrep_sst:wsrep_sst';"
mysql -e "CREATE USER 'wsrep_sst'@'localhost' IDENTIFIED BY 'wsrep_sst';"
mysql -e "GRANT RELOAD, PROCESS, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'wsrep_sst'@'localhost';"
#show cluster status
mysql -e "SHOW GLOBAL STATUS LIKE 'wsrep_cluster_size';"
tune2fs -m1 /dev/sda3
tune2fs -m0 /dev/sdb
