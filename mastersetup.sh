#/bin/bash
mysql -e "GRANT REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'maslave'@'%' IDENTIFIED BY 'mapass';"
echo " run on the SLAVE"
echo "sed -i '/wsrep_/d' /etc/mysql/conf.d/galera.cnf"
echo "systemctl restart mariadb.service"
echo "mysql -e \"change master to master_host='$(hostname -I | awk '{print $1}')', master_user='maslave', master_password='mapass', master_log_file='$(mysql -e "SHOW MASTER STATUS\G" | grep File: | awk '{ print $2}' | tr '\n' ' ')', master_log_pos=$(mysql -e "SHOW MASTER STATUS\G" | grep Position: | awk '{ print $2}' | tr '\n' ' ');\""
echo "mysql -e \"start slave;\""
echo "if slave drama on init - reset slave;"
