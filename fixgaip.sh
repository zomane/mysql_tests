#!/bin/bash
sed -i "s/.*wsrep_node_address.*/wsrep_node_address     = \"$(hostname -I | awk '{print $1}')\"/" /etc/mysql/conf.d/galera.cnf
sed -i "s/.*server_id.*/server_id     		= $(hostname | tail -c 2)/" /etc/mysql/conf.d/galera.cnf
