#!/bin/bash
terraform apply -auto-approve
terraform show | grep ipv4_address | awk '{ print $3}' | perl  -p -e 's/\"//g' > inventory.yml
sed -i "s/.*SEDMATCHSTRING.*/iptables -A INPUT -s $(cat inventory.yml  | tr '\n' ',' | sed 's/,*$//g')  -m comment --comment "SEDMATCHSTRING" -j ACCEPT/" fw.sh
sed -i "s#.*wsrep_cluster_address.*#wsrep_cluster_address  = \"gcomm://$(cat inventory.yml  | tr '\n' ',' | sed 's/,*$//g')\"#" 61-galera.cnf
# hetzner VM's sometimes are spawning slow - wait a bit
sleep 12
ANSIBLE_RETRY_FILES_ENABLED=0 ansible-playbook  --verbose --user=root -i inventory.yml dbcluster.yml
