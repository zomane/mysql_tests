---
- name: basic setup
  hosts: all

  tasks:
    - name: maria repo apt key
      apt_key:
        state: present
        url:   https://mariadb.org/mariadb_release_signing_key.asc

    - name: maria repo
      apt_repository:
        repo: "deb [arch=amd64,i386,ppc64el] https://mirror.klaus-uwe.me/mariadb/repo/10.1/debian {{ ansible_distribution_release }} main"
        state: present
        update_cache: yes
        filename: php

    - name: apt keys
      apt_key:
        state:     present
        url:       "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x9334A25F8507EFA5"
        # url2 https://percona.com/downloads/deb-percona-keyring.gpg

    - name: precona repo
      apt_repository:
        repo:         "deb http://repo.percona.com/apt {{ ansible_distribution_release }} main"
        state:        present
        filename:     percona
        update_cache: yes

    - name: rc.local
      copy:
        src: rc.local
        dest: /etc/rc.local
        mode: 0755

    - name: the FW
      copy:
        src: fw.sh
        dest: /usr/local/bin/fw.sh
        mode: 0755

    - name: ssh autkeys
      copy:
        src: authkeys
        dest: /root/.ssh/authorized_keys
        mode: 0600

    - name: ssh key
      copy:
        src: id_rsa
        dest: /root/.ssh/id_rsa
        mode: 0600

    - name: ssh key
      copy:
        src: id_rsa.pub
        dest: /root/.ssh/id_rsa.pub
        mode: 0644

    - name: enable rc.local
      systemd:
        name: rc.local
        enabled: yes
        state: started

    - name: install packages
      su: true
      apt: name="{{item}}" state=present
      with_items:
        - mc
        - vim
        - parallel
        - screen
        - tmux
        - sudo
        - telnet
        - mariadb-server
        - percona-xtrabackup-24
        - bash-completion
        - netcat
        - mariadb-backup-10.1
        - nload
        - socat
        - less 
        - etckeeper

    - name: galera
      copy:
        src: 61-galera.cnf
        dest: /etc/mysql/conf.d/galera.cnf

    - name: mysqldcfg
      copy:
        src: mysqld.cnf
        dest: /etc/mysql/conf.d/mysqld.cnf

    - name: link mysqld
      file:
        src: /etc/mysql/conf.d/mysqld.cnf
        dest: /etc/mysql/mariadb.conf.d/60-mysql.cnf
        state: link

    - name: iinodbopts
      copy:
        src: innodb.cnf
        dest: /etc/mysql/conf.d/innodb.cnf

    - name: link mysqld
      file:
        src: /etc/mysql/conf.d/innodb.cnf
        dest: /etc/mysql/mariadb.conf.d/70-innodb.cnf
        state: link

    - name: fix missing mariadb.service.d in the deb repo package
      file:
        path: /etc/systemd/system/mariadb.service.d/
        state: directory
        mode: '0755'

    - name: service_timeout
      copy:
        src: maria_timeout.conf
        dest: /etc/systemd/system/mariadb.service.d/timeoutsec.conf

#    - name: change mysql datadir
#      lineinfile:
#        state: present
#        path: /etc/mysql/my.cnf
#        regexp: '^datadir'
#        line: datadir       = /srv/mysql/data

    - name: max_allowed_packet
      replace:
        path: /etc/mysql/my.cnf
        regexp: '^max_allowed_packet.*'
        replace: 'max_allowed_packet	= 512M'

    - name: fix the IP in galera- ansible escapes the commands so we need a script
      copy:
        src: fixgaip.sh
        dest: /usr/local/bin/fixgaip.sh
        mode: 0755

    - name: set proper ip for galera
      command: /usr/local/bin/fixgaip.sh 

    - name: fix vim
      copy:
        src: vimrc.local
        dest: /etc/vim/vimrc.local

    - name: maria sysd timeouts fix
      lineinfile:
        path: /lib/systemd/system/mariadb.service
        regexp: '^TimeoutStartSec='
        line: TimeoutStartSec=infinity

    - name: maria sysd timeouts fix
      lineinfile:
        path: /lib/systemd/system/mariadb.service
        regexp: '^TimeoutStopSec='
        line: TimeoutStopSec=infinity 

    - name: issue daemon-reload after maria sysd timeouts changes 
      systemd:
        daemon_reload: yes

#    - name: restart mariadb after cfg change 
#      systemd:
#        state: stopped
#        name: mariadb 
#
#    - name: this initializes the cluster run on the first one node - no run no cluster = mucho swearing 
#      copy:
#        src: setup_the_cluster_run_on_ONE_node.sh
#        dest: /usr/local/bin/setup_the_cluster_run_on_ONE_node.sh
#        mode: 0755
#
    - name: copy the cluster scripts
      copy:
        src: "{{ item }}"
        dest: /usr/local/bin/ 
        mode: 0755

      loop:
         - setup_the_cluster_run_on_the_slaves.sh
         - setup_the_cluster_run_on_ONE_node.sh
         - importdump.sh
         - dbupgrade.sh
         - mastersetup.sh

    - name: mount the volume 
      mount:
        path: /srv/mysql
        src: /dev/sdb
        fstype: ext4
        state: mounted

    - name: fix perms new mysql datadir
      file:
        path: /srv/mysql/data
        state: directory
        mode: '0755'
        owner: mysql
        group: mysql

    - name: mysql logdir
      file:
        path: /srv/mysql/log
        state: directory
        mode: '0755'
        owner: mysql
        group: mysql

    - name: mysql tmp
      file:
        path: /srv/mysql/tmp
        state: directory
        mode: '0755'
        owner: mysql
        group: mysql
