# mysql_tests
I did that thing to prepare and test some DB stuff at work
but I think someone else also might find it usefull
Keep in mind that it's ugly and will stay ugly i.e. the mix of
SQL, bash, ansible, terraform and whatever that might come later may give you nightmares so...
...better not use in production because it lacks security improvements and many error checks

Basically it is a bunch of scripts & stuff to setup 
mariadb cluster and master-slave testground for various experiments 
To use it you will need terraform with hcloud plugin, hcloud_token and ansible
Because it is initially created to be used with Hetzner's cloud some stuff is hardcoded to it

The final setup of the cluster and the slave is intentionally manual
It is done by:
setup_the_cluster_run_on_ONE_node.sh - on the "first" cluster node
setup_the_cluster_run_on_the_slaves.sh - on all of the other cluster nodes
mastersetup.sh - the output from here needs to be executed on the slave
importdump.sh - on any of the cluster nodes

executed in that order
