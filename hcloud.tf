variable "hcloud_token" {}

provider "hcloud" {
  token = "${var.hcloud_token}"
}

data "hcloud_ssh_key" "ssh_key" {
  fingerprint = "the:key:finger:print:here"
}

# intentionally repetitive declarations instead of a loop
# for some tests distros or hosts specs needs to be different
resource "hcloud_server" "mdb0" {
  name        = "mdb0"
  image       = "debian-9"
  server_type = "cpx21"
  ssh_keys    = ["${data.hcloud_ssh_key.ssh_key.id}"]
}
resource "hcloud_server" "mdb1" {
  name        = "mdb1"
  image       = "debian-9"
  server_type = "cpx21"
  ssh_keys    = ["${data.hcloud_ssh_key.ssh_key.id}"]
}
resource "hcloud_server" "mdb2" {
  name        = "mdb2"
  image       = "debian-9"
  server_type = "cpx21"
  ssh_keys    = ["${data.hcloud_ssh_key.ssh_key.id}"]
}

resource "hcloud_server" "mdb3" {
  name        = "mdb3"
  image       = "debian-9"
  server_type = "cpx21"
  ssh_keys    = ["${data.hcloud_ssh_key.ssh_key.id}"]
}

resource "hcloud_volume" "mdb0v" {
  name       = "maria0"
  size       = 150
  server_id  = "${hcloud_server.mdb0.id}"
  format     = "ext4"
}
resource "hcloud_volume" "mdb1v" {
  name       = "maria1"
  size       = 150
  server_id  = "${hcloud_server.mdb1.id}"
  format     = "ext4"
}
resource "hcloud_volume" "mdb2v" {
  name       = "maria2"
  size       = 150
  server_id  = "${hcloud_server.mdb2.id}"
  format     = "ext4"
}

resource "hcloud_volume" "mdb3v" {
  name       = "maria3"
  size       = 150
  server_id  = "${hcloud_server.mdb3.id}"
  format     = "ext4"
}
